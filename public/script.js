document.getElementById('send-button').addEventListener('click', sendMessage);
document.getElementById('user-input').addEventListener('keypress', function (e) {
    if (e.key === 'Enter') {
        sendMessage();
    }
});

function sendMessage() {
    const userInput = document.getElementById('user-input');
    const message = userInput.value.trim();
    if (message === '') return;

    addMessageToChat('userMessage', 'You', message);
    userInput.value = '';

    setTimeout(() => {
        addMessageToChat('botMessage', 'Bot', 'Yes.');
    }, 5000);
}

function addMessageToChat(className, sender, message) {
    const chatBox = document.getElementById('chat-box');
    const messageElement = document.createElement('div');
    messageElement.className = `message ${className}`;

    const labelElement = document.createElement('span');
    labelElement.className = 'message-label';
    labelElement.textContent = `${sender}:`;

    const textElement = document.createElement('span');
    textElement.textContent = ` ${message}`;

    messageElement.appendChild(labelElement);
    messageElement.appendChild(textElement);
    chatBox.appendChild(messageElement);
    chatBox.scrollTop = chatBox.scrollHeight;
}
